// import { useState, useEffect } from 'react';
import { Card, Col } from 'react-bootstrap';
import { Link } from 'react-router-dom';

export default function ProfileCard(props) {
    // console.log(courseProp)
    // console.log(props);
    // result: coursesData[0]- wdc001
    // console.log(typeof props);
    // result: object

    //object destructuring
    const {breakpoint, userProp} = props
    const {email, firstName, lastName} = userProp
    // console.log(name);

    //Syntax: const [getter, setter] = useState(initialValueOfGetter)
    // const [count, setCount] = useState(0)
    // const [seats, setSeats] = useState(30)
    // const [isOpen, setIsOpen] = useState(false)

    // const enroll = () => {
    //     if (seats > 0) {
    //         setCount(count + 1);
    //         console.log('Enrollees: ' + count);

    //         setSeats(seats - 1);
    //         console.log('Seats: ' + seats);
    //     } else {
    //         alert("No more seats available");
    //     };
    // }

    // useEffect(() => {
    //     if(seats === 0){
    //         setIsOpen(true)
    //     }
    // }, [seats])

    return (
        <Col xs={12} md={breakpoint} className="mt-4">
        <Card className="card1">
            <Card.Body>
                <Card.Title className="text-center card2">{email}</Card.Title>
                <Card.Subtitle>Description:</Card.Subtitle>
                <Card.Text className="card3">{firstName}</Card.Text>
                <Card.Subtitle>Price:</Card.Subtitle>
                <Card.Text>{lastName}</Card.Text>
                
            </Card.Body>
        </Card>
        </Col>
   
    )
}
