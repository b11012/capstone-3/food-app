import {Carousel} from 'react-bootstrap'

export default function McDollibeeCarousel(){


	return(
		<Carousel>
		  <Carousel.Item>
		    <img
		      className="d-block w-100"
		      src="https://imagesvc.meredithcorp.io/v3/mm/image?url=https%3A%2F%2Fstatic.onecms.io%2Fwp-content%2Fuploads%2Fsites%2F9%2F2021%2F07%2F13%2FUltimate-Veggie-Burgers-FT-Recipe-0821.jpg&q=60"
		      alt="First slide"
		    />
		  </Carousel.Item>
		  <Carousel.Item>
		    <img
		      className="d-block w-100"
		      src="https://static.fanpage.it/wp-content/uploads/sites/22/2021/06/spaghetti-bolognese-1200x900.jpg"
		      alt="Second slide"
		    />
		  </Carousel.Item>
		  <Carousel.Item>
		    <img
		      className="d-block w-100"
		      src="https://www.seriouseats.com/thmb/7Mgvc9lUosqYHJ9JgAVAGWqQMdg=/1500x1125/filters:fill(auto,1)/__opt__aboutcom__coeus__resources__content_migration__serious_eats__seriouseats.com__2020__10__20201002-mission-style-burrito-jillian-atkinson-2-6841455590ed4c3981529b232166643a.jpg"
		      alt="Third slide"
		    />
		  </Carousel.Item>
		</Carousel>
	)
}