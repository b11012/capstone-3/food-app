// import coursesData from '../data/coursesData';
import {Row} from 'react-bootstrap'
import { useEffect, useState } from 'react'
import ProductCard from '../components/ProductCard';

export default function Products({product}){

	// console.log(coursesData);
	// holds the mock database
	// console.log(coursesData[0]);
	// captured the first object in coursesData

	// const courses = coursesData.map(course => {
	// 	console.log(course.id)
	// 	return(
	// 		<CourseCard key = {course.id} courseProp = {course}/>
	// 	)
	// })

	const [products, setProducts] = useState([])

	useEffect(() => {
		fetch('https://gentle-atoll-71855.herokuapp.com/products')
		.then(res => res.json())
		.then(data => {
			console.log(data)

			const productsArr = (data.map(product => {

				return (
					<ProductCard key={products._id} productProp={product} breakpoint={4} />
				)
				
			}))
			setProducts(productsArr)
		})

	}, [product])

	return(
	<>
		<Row>
		<h1> Order Now! </h1>
			{products}
		</Row>

	</>
	)
}

 