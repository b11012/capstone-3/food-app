
import Highlights from '../components/Highlights';
import McDollibeeCarousel from '../components/McDollibeeCarousel'

export default function Home() {
    return (
        <>
        <h1>Welcome to McDollibee!</h1>
            <McDollibeeCarousel/>
	        <Highlights />
		</>

    )
}
