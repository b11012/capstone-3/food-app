import { useState, useEffect, useContext } from 'react';
import { Form, Button } from 'react-bootstrap';
import { useParams, Navigate, useNavigate } from 'react-router-dom';
import Swal from 'sweetalert2';
import UserContext from '../UserContext';

export default function Add(){

	const { user } = useContext(UserContext)

	const history = useNavigate();

	const { productId } = useParams();

	const [name, setName] = useState('');
	const [description, setDescription] = useState('');
	const [price, setPrice] = useState(0);
		const [isActive, setIsActive] = useState(false);

	console.log(name);
	console.log(description);
	console.log(price);

	useEffect(() => {

		if(name !== '' && description !== '' && price !== ''){

			setIsActive(true);
		}

			setIsActive(false);
	})

	function AddingProduct(e) {

	e.preventDefault();

	fetch('https://gentle-atoll-71855.herokuapp.com/products', {

		method: 'POST',
		headers: {'Content-Type' : 'application/json',
				Authorization: `Bearer ${localStorage.getItem('token')}`
			},

			body: JSON.stringify({
				name: name,
				description: description,
				price: price
	})
})

	.then(res => res.json())
	.then(data => {
		console.log(data);


			if(data){
				Swal.fire({
					title: 'Successfully Enrolled',
					icon: 'success',
					text: 'Thank you for enrolling!'
				})
				
				history("/homepage");

			} else {

				Swal.fire({
					title: 'Something went wrong',
					icon: 'error',
					text: 'Please try again later'
				})
			}
		})

	setName('');
	setDescription('');
	setPrice('');
}

return (
	<>
	<h1>Add Product Here:</h1>
	<Form>

		<Form.Group controlId="name">
			<Form.Label>Name:</Form.Label>
			<Form.Control
				type="text"
				placeholder="Please input your first name here"
				required
				value={name}
				onChange={e => setName(e.target.value)}
			/>
		</Form.Group>

		<Form.Group controlId="name">
			<Form.Label>Name:</Form.Label>
			<Form.Control
				type="text"
				placeholder="Please input your first name here"
				required
				value={description}
				onChange={e => setDescription(e.target.value)}
			/>
		</Form.Group>

		<Form.Group controlId="name">
			<Form.Label>Name:</Form.Label>
			<Form.Control
				type="text"
				placeholder="Please input your first name here"
				required
				value={price}
				onChange={productId => setPrice(productId.target.value)}
			/>
		</Form.Group>
	
	</Form>
	</>

)
}