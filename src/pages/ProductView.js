import { useState, useEffect, useContext } from 'react';
import {Container, Row, Col, Card, Button} from 'react-bootstrap';
import { useParams, useNavigate, Link } from 'react-router-dom';
import Swal from 'sweetalert2';
import UserContext from '../UserContext'

export default function ProductView(){

	const { user } = useContext(UserContext);

	const history = useNavigate();

	const { productId } = useParams();

	const [name, setName] = useState("");
	const [description, setDescription] = useState("");
	const [price, setPrice] = useState(0);

	const [count, setCount] = useState(0)
	const [isOpen, setIsOpen] = useState(false)

	const numberOfItem = () => {
	        setCount(count + 1);
	        console.log('Order: ' + count);

	    }
	  

	const Ordering = (productId) => {

		fetch('https://gentle-atoll-71855.herokuapp.com/users/order', {

			method: 'POST',
			headers: {
				'Content-Type' : 'application/json',
				Authorization: `Bearer ${localStorage.getItem('token')}`
			},
			body: JSON.stringify({
				productId: productId
			})
		})
		.then(res => res.json())
		.then(data => {
			console.log(data);

			if(data){
				Swal.fire({
					title: 'Added to cart',
					icon: 'success',
					text: 'Thank you for your purchase!'
				})
				
				history("/ordering");

			} else {

				Swal.fire({
					title: 'Something went wrong',
					icon: 'error',
					text: 'Please try again later'
				})
			}
		})
	}

	useEffect(() => {
		console.log(productId)

		fetch(`https://gentle-atoll-71855.herokuapp.com/products/getSingleProduct/${productId}`)
		.then(res => res.json())
		.then(data => {
			console.log(data)

			setName(data.name)
			setPrice(data.price)
			setDescription(data.description)

		})

	}, [productId])


	return (

		<Container className="mt-5">
			<Row>
				<Col lg={{span: 6, offset: 3}}>
					<Card>
						<Card.Body>
							<Card.Title>{name}</Card.Title>

							<Card.Subtitle>Description:</Card.Subtitle>
							<Card.Text>{description}</Card.Text>

							<Card.Subtitle>Price:</Card.Subtitle>
							<Card.Text>{price}</Card.Text>

							<Card.Subtitle>No. of items:</Card.Subtitle>
							<Card.Text>{count}</Card.Text>
							<Button variant="primary" onClick={numberOfItem}>How Many?</Button>

						{ user.id !== null ?	
							<Button variant="primary" onClick={() => Ordering(productId)}>Add to cart</Button>
							:

							<Link className="btn btn-danger" to="/login">Log In</Link>
						}
						

						</Card.Body>
					</Card>
				</Col>
			</Row>
		</Container>

	)
}